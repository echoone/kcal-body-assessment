<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MultiStepForm extends Component
{

    public $step = 0;

    public  $name = '',
            $gender = 'male',
            $age = 38,
            $weight = 100,
            $weight_unit = 'kg',
            $height = 172,
            $height_unit = 'cm',
            $shape = 'mesomorph',
            $health_goals = '',
            $weight_goals = 80,
            $activity = 'moderate',
            $exercise = [],
            $exercise_other,
            $exercise_none_reason = [],
            $exercise_none_reason_other,
            $medical_risk = [],
            $medication = false,
            $medication_list = '',
            $medical_diet = false,
            $medical_diet_list = '',
            $vegitable_dislikes = [],
            $protein_dislikes = [],
            $allergies = [],
            $beverages = [],
            $alcohol_frequency = '',
            $calories = 0,
            $first_name,
            $surname,
            $mobile,
            $email;

    private $stepActions = [
        'submit_step_0',
        'submit_step_1',
        'submit_step_2',
        'submit_step_3',
        'submit_step_4',
        'submit_step_5',
        'submit_step_6',
        'submit_step_7',
        'submit_step_8',
        'submit_step_9',
        'submit_step_10',
        'submit_step_11',
        'submit_step_12',
        'submit_step_13',
        'submit_step_14',
        'submit_step_15',
        'submit_step_16',
    ];

    protected $rules = [
        'name' => 'required',
        'age' => 'required|int',
        'weight' => 'required',
        'height' => 'required',
        'health_goals' => 'required',
        'weight_goals' => 'required',

        'first_name' => 'required',
        'surname' => 'required',
        'mobile' => 'required',
        'email' => 'required',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }


    public function render()
    {
        return view('livewire.multi-step-form');
    }

    public function submit()
    {
        $action = $this->stepActions[$this->step];

        $this->$action();
    }

    public function submit_step_0()
    {
        $this->step = 1;
    }

    public function submit_step_1()
    {
        
        $this->validate([
            'name' => 'required',
        ]);

        $this->step++;
    }

    public function submit_step_2()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }

    public function submit_step_3()
    {

        $this->validate([
            'age' => 'required|int',
            'weight' => 'required',
            'height' => 'required',
        ]);

        $this->step++;
    }

    public function submit_step_4()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        

        $this->step++;
    }

    public function submit_step_5()
    {

        $this->validate([
            'health_goals' => 'required',
            'weight_goals' => 'required',
        ]);
        $this->step++;
    }

    public function submit_step_6()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);
        $this->step++;
    }

    public function submit_step_7()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }

    public function submit_step_8()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }

    public function submit_step_9()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }

    public function submit_step_10()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }

    public function submit_step_11()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);
        $this->step++;
    }

    public function submit_step_12()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);
        $this->step++;
    }

    public function submit_step_13()
    {

        $bmr = 0;
        $tee = 0;

        if($this->gender == 'female') {
            $bmr = (10 * $this->weight) + (6.25 * $this->height) + (5 * $this->age) - 161;
        } else {
            $bmr = (10 * $this->weight) + (6.25 * $this->height) + (5 * $this->age) + 5;
        }
        

        switch ($this->activity) {
            case 'seditary':
                $tee = $bmr + 200;
                break;
            case 'lightly_active':
                $tee = $bmr + 400;
                break;
            case 'moderate':
                $tee = $bmr + 520;
                break;
            case 'very_active':
                $tee = $bmr + 700;
                break;
            case 'extremely_active':
                $tee = $bmr + 1000;
                break;
        }

        switch ($this->health_goals) {
            case 'weight_loss':
                $this->calories = ceil(($tee - 800)) . ' - ' . ceil(($tee - 500));
                break;

            case 'maintain_weight':
                $this->calories = ceil(($tee - 100)) . ' - ' . ceil(($tee + 100));
                break;

            case 'build_muscle':
                $this->calories = ceil(($tee + 400)) . ' - ' . ceil(($tee + 600));
                break;
            
            default:
                $this->calories = ceil($tee);
                break;
        }

        $this->step++;
    }

    public function submit_step_14()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
    }
    public function submit_step_15()
    {

        // $this->validate([
        //     //'name' => 'required',
        // ]);

        $this->step++;
        //$this->step == 'complete';
    }
    public function submit_step_16()
    {

        $this->validate([
            'first_name' => 'required',
            'surname' => 'required',
            'mobile' => 'required',
            'email' => 'required',
        ]);

        $this->step = 'complete';
    }

    public function change_shape_left() {
        
        if($this->shape == 'mesomorph') {
            $this->shape = 'endomorph';
        } elseif ($this->shape == 'endomorph') {
            $this->shape = 'ectomorph';
        } elseif ($this->shape == 'ectomorph') {
            $this->shape = 'mesomorph';
        }
    }

    public function change_shape_right()
    {
        if ($this->shape == 'mesomorph') {
            $this->shape = 'endomorph';
        } elseif ($this->shape == 'endomorph') {
            $this->shape = 'ectomorph';
        } elseif ($this->shape == 'ectomorph') {
            $this->shape = 'mesomorph';
        }
    }

    public function change_activity_left()
    {

        if ($this->activity == 'seditary') {
            $this->activity = 'extremely_active';
        } elseif ($this->activity == 'lightly_active') {
            $this->activity = 'seditary';
        } elseif ($this->activity == 'moderate') {
            $this->activity = 'lightly_active';
        } elseif ($this->activity == 'very_active') {
            $this->activity = 'moderate';
        } elseif ($this->activity == 'extremely_active') {
            $this->activity = 'very_active';
        }
    }

    public function change_activity_right()
    {
        if ($this->activity == 'seditary') {
            $this->activity = 'lightly_active';
        } elseif ($this->activity == 'lightly_active') {
            $this->activity = 'moderate';
        } elseif ($this->activity == 'moderate') {
            $this->activity = 'very_active';
        } elseif ($this->activity == 'very_active') {
            $this->activity = 'extremely_active';
        } elseif ($this->activity == 'extremely_active') {
            $this->activity = 'seditary';
        }
    }

    public function toggle_value($array_name, $value)
    {
        if(in_array($value, $this->$array_name)) {
            $key = array_search($value, $this->$array_name);
            unset($this->$array_name[$key]);
        } else {
            $this->$array_name[] = $value;
        }
    }
}
