<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'gender',
        'age',
        'weight',
        'weight_unit',
        'height',
        'height_unit',
        'shape',
        'health_goals',
        'weight_goals',
        'activity',
        'exercise',
        'exercise_other',
        'exercise_none_reason',
        'exercise_none_reason_other',
        'medical_risk',
        'medication',
        'medication_list',
        'medical_diet',
        'medical_diet_list',
        'vegitable_dislikes',
        'protein_dislikes',
        'allergies',
        'beverages',
        'alcohol_frequency',
        'calories',
        'first_name',
        'surname',
        'mobile',
        'email',
    ];
}

