<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('weight')->nullable();
            $table->string('weight_unit')->nullable();
            $table->string('height')->nullable();
            $table->string('height_unit')->nullable();
            $table->string('shape')->nullable();
            $table->string('health_goals')->nullable();
            $table->string('weight_goals')->nullable();
            $table->string('activity')->nullable();
            $table->string('exercise')->nullable();
            $table->string('exercise_other')->nullable();
            $table->string('exercise_none_reason')->nullable();
            $table->string('exercise_none_reason_other')->nullable();
            $table->string('medical_risk')->nullable();
            $table->boolean('medication');
            $table->string('medication_list')->nullable();
            $table->boolean('medical_diet');
            $table->string('medical_diet_list')->nullable();
            $table->string('vegitable_dislikes')->nullable();
            $table->string('protein_dislikes')->nullable();
            $table->string('allergies')->nullable();
            $table->string('beverages')->nullable();
            $table->string('alcohol_frequency')->nullable();
            $table->string('calories')->nullable();
            $table->string('first_name')->nullable();
            $table->string('surname')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
