<div class="flex flex-col h-full justify-center items-center">
    <div class="my-5 w-full ">
        <h1 class="text-white text-5xl md:text-7xl text-left">
            Thank you!
        </h1>
    </div>
    <div class="mb-8 mt-5 w-full">
        <p class="text-kcal-green-dark text-3xl md:text-3xl text-left mb-5">
            An email has been sent to your inbox.
        </p>
    </div>
    <div class="mb-5 mt-5 w-full">
        <p class="text-kcal-green-dark text-2xl md:text-xl text-left mb-5">
            Once you recieve your report you can book a chat with one of our nutritionists and get started with your perfect meal plan!
        </p>
        <p class="text-kcal-green-dark text-2xl md:text-xl text-left">
            If you have any problems don’t hesitate to get in touch.
        </p>
    </div>
    <div class="my-10 w-full">
        <div class="w-full text-center">
            <button @click="step = 0" class="w-full md:w-60 h-12 uppercase tracking-wider focus:outline-none border border-transparent py-3 px-5 rounded-lg text-center bg-white text-kcal-green-dark font-semibold">Restart</button>
        </div>
    </div>
</div>