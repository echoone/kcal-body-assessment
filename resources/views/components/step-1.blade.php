<div class="flex flex-col h-full justify-center items-center">
    <div class="my-10 w-full ">
        <h1 class="text-kcal-green text-5xl md:text-7xl text-left">
            Welcome to<br />the Body Profile<br />Assessment. <br />Let’s get started.
        </h1>
    </div>
    <div class="mx-7  w-full">
        <label for="name" class="font-semibold mb-2 block uppercase text-kcal-green-dark tracking-wide">What do your friends call you?</label>
        <input type="text" wire:model="name" class="w-full px-4 py-3 rounded-lg focus:outline-none focus:ring-kcal-orange border border-transparent focus:border-kcal-orange text-kcal-orange font-semibold bg-kcal-grey-light text-xl" placeholder="">
        @error('name') <span class="error uppercase text-red-400 mt-2 font-semibold block text-sm">{{ $message }}</span> @enderror
    </div>
    <div class="mb-5 mt-14">
        <p class="text-kcal-green text-2xl md:text-3xl">
            At the end of the quiz you will receive your full report, a FREE gymnation pass, your FREE workout guide and a sample meal plan!
        </p>
    </div>
</div>