<div class="flex flex-col md:flex-row md:items-center md:justify-between">
    <div class="flex items-center w-full">
        <div class="w-full bg-kcal-grey-light rounded-full mr-2 relative">
            <div class="rounded-full bg-kcal-green text-xs leading-none h-2 text-center text-white ring-2 ring-white transition-all duration-1000" :style="'width: '+ parseInt((step - 1) / 13 * 100) +'%'"></div>
            <div class="text-sm w-10  absolute -top-1 bg-white transition-all duration-1000" x-show="step > 1" :style="'left: '+ parseInt((step - 1) / 13 * 100) +'%'">
                <span x-text="parseInt((step - 1) / 13 * 100) +'%'" class="text-center font-semibold text-kcal-green w-full block"></span>
                <span class="absolute -right-1 top-1 w-2 h-2 bg-kcal-grey-light rounded-full"></span>
            </div>
            <div class="absolute right-0 -top-1 w-3 h-3 ring-kcal-grey-light bg-white rounded-full ring-8">
            </div>
        </div>
    </div>
</div>