const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        fontWeight: {
            light: 300,
            normal: 400,
            semibold: 600,
            extrabold: 800,
        },
        extend: {
            fontFamily: {
                sans: ['ItalianPlateNo1', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                'kcal-green-lighter': '#F7FCFC',
                'kcal-green-light': '#B5E3D8',
                'kcal-green': '#9ABEAA',
                'kcal-green-dark': '#154734',
                'kcal-grey-lighter': '#FAF8FA',
                'kcal-grey-light': '#F0F0F0',
                'kcal-grey': '#A7A7A7',
                'kcal-grey-dark': '#707070',
                'kcal-orange': '#EFBE7D',
                'kcal-red': '#DDA69D',
                'kcal-brown': '#CDC4A9',
                'kcal-purple': '#D0B0CA',
                'kcal-purple-dark': '#D0B0CA',
                'kcal-step-0': '#B5E3D8',
                'kcal-step-2': '#D0B0CA',
                'kcal-step-6': '#9ABEAA',
                'kcal-step-12': '#DDA69D',
                'kcal-step-complete': '#B5E3D8',
            },
            backgroundImage: theme => ({
                'step-0-bg': "url('/img/step-0-bg.svg')",
                'main-bg': "url('/img/main-bg.svg')",
            })
        },
    },

    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
